package com.kh.subproject.board.repository;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.kh.subproject.board.vo.Article;
import com.kh.subproject.board.vo.Board;

@Service
@Transactional
public class BoardRepositoryServiceImpl implements BoardRepositoryService{
	
	@Autowired
	private BoardRepository br;
	
	public BoardRepositoryServiceImpl() {}

	@Override
	public Board addBoard(Board board) {
		// TODO Auto-generated method stub
		board.setbStatus("Y");
		return br.save(board);
	}

	@Override
	public Page<Board> selectBoardList(Pageable pageable) {
		// TODO Auto-generated method stub
		return br.findAll(pageable);
	}

	@Override
	public void deleteBoard(Board board) {
		// TODO Auto-generated method stub
		br.delete(board);;
	}

	@Override
	public Board boardUpdate(Board board) {
		// TODO Auto-generated method stub
		return br.save(board);
	}
	
	
	
}
