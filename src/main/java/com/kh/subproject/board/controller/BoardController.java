package com.kh.subproject.board.controller;

import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import com.kh.subproject.board.repository.BoardRepositoryService;
import com.kh.subproject.board.vo.Article;
import com.kh.subproject.board.vo.Board;

@Controller
public class BoardController {
	
	@Autowired
	private BoardRepositoryService brs; 
	
	@RequestMapping("/board/boardMain")
	public String board () {
		return "board/boardMain";
	}
	
	@RequestMapping(value="/board/add")
	public String add(@RequestBody Board board) {
		System.out.println(board);
		Board entity = brs.addBoard(board);
	  
	    return "board/boardMain";
	}
	
	@GetMapping(value="/board/boardList")
	public @ResponseBody Page<Board> selectBoardList(Pageable pageable){
		
		Page<Board> list = brs.selectBoardList(pageable);
		System.out.println(list);
		return list;
	}
	
	@PostMapping(value="/board/boardDelete")
	public @ResponseBody String deleteBoard(@RequestBody Board board) {
		System.out.println(board);
		brs.deleteBoard(board);
		return "ok";
	}
	
	@PostMapping(value="/board/boardUpdate")
	public @ResponseBody Board boardUpdate(@RequestBody Board board) {
		System.out.println(board);
		Board result = brs.boardUpdate(board);
		return result;
	}
}























