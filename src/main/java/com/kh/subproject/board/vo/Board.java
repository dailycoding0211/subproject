package com.kh.subproject.board.vo;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;

import javax.persistence.*;
import java.util.Date;
import java.util.List;

@Entity
@Data
@NoArgsConstructor
@AllArgsConstructor
@Getter
@Setter
public class Board {
	
	public Board () {}
	
    
    public Board(Long bno, String bName, String bTitle, String bContent, Date bDate, String bStatus,
			List<Article> article) {
		super();
		this.bno = bno;
		this.bName = bName;
		this.bTitle = bTitle;
		this.bContent = bContent;
		this.bDate = bDate;
		this.bStatus = bStatus;
		this.article = article;
	}


	@Id @GeneratedValue
    private Long bno;

    private String bName;

//    private String bType;
    
    private String bTitle;
    
    @Column(length = 1377)
    private String bContent;

    @Temporal(TemporalType.TIMESTAMP)
    private Date bDate;

    private String bStatus;

    @OneToMany(mappedBy = "bno")
    private List<Article> article;

	@Override
	public String toString() {
		return "Board [bno=" + bno + ", bName=" + bName + ", bTitle=" + bTitle + ", bContent=" + bContent + ", bDate="
				+ bDate + ", bStatus=" + bStatus + ", article=" + article + "]";
	}


	public Long getBno() {
		return bno;
	}


	public void setBno(Long bno) {
		this.bno = bno;
	}


	public String getbName() {
		return bName;
	}


	public void setbName(String bName) {
		this.bName = bName;
	}


	public String getbTitle() {
		return bTitle;
	}


	public void setbTitle(String bTitle) {
		this.bTitle = bTitle;
	}


	public String getbContent() {
		return bContent;
	}


	public void setbContent(String bContent) {
		this.bContent = bContent;
	}


	public Date getbDate() {
		return bDate;
	}


	public void setbDate(Date bDate) {
		this.bDate = bDate;
	}


	public String getbStatus() {
		return bStatus;
	}


	public void setbStatus(String bStatus) {
		this.bStatus = bStatus;
	}


	public List<Article> getArticle() {
		return article;
	}


	public void setArticle(List<Article> article) {
		this.article = article;
	}

    
    
    
}
