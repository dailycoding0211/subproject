package com.kh.subproject.board.vo;

import com.kh.subproject.member.vo.Member;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import java.util.Date;
import java.util.List;

@Entity
public class Comments {
	
	public Comments() {}
	
    public Comments(Long cno, String cContent, Date cDate, Member mno, String cStatus, Article article,
			Comments superComment, List<Comments> replies) {
		super();
		this.cno = cno;
		this.cContent = cContent;
		this.cDate = cDate;
		this.mno = mno;
		this.cStatus = cStatus;
		this.article = article;
		this.superComment = superComment;
		this.replies = replies;
	}

	@Id
    @GeneratedValue
    private Long cno;

    private String cContent;

    @Temporal(TemporalType.TIMESTAMP)
    private Date cDate;

    @ManyToOne
    private Member mno;
    
    private String cStatus;

    @ManyToOne
    private Article article;

    @ManyToOne
    private Comments superComment;

    @OneToMany(mappedBy = "superComment")
    private List<Comments> replies;

	public Long getCno() {
		return cno;
	}

	public void setCno(Long cno) {
		this.cno = cno;
	}

	public String getcContent() {
		return cContent;
	}

	public void setcContent(String cContent) {
		this.cContent = cContent;
	}

	public Date getcDate() {
		return cDate;
	}

	public void setcDate(Date cDate) {
		this.cDate = cDate;
	}

	public Member getMno() {
		return mno;
	}

	public void setMno(Member mno) {
		this.mno = mno;
	}

	public String getcStatus() {
		return cStatus;
	}

	public void setcStatus(String cStatus) {
		this.cStatus = cStatus;
	}

	public Article getArticle() {
		return article;
	}

	public void setArticle(Article article) {
		this.article = article;
	}

	public Comments getSuperComment() {
		return superComment;
	}

	public void setSuperComment(Comments superComment) {
		this.superComment = superComment;
	}

	public List<Comments> getReplies() {
		return replies;
	}

	public void setReplies(List<Comments> replies) {
		this.replies = replies;
	}
    
    
}
