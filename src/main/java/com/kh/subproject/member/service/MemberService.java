package com.kh.subproject.member.service;

import com.kh.subproject.member.vo.Member;

import java.util.Optional;

public interface MemberService {
    String joinMember(Member member);

    Member loginMember(Member member);

    Member selectMember(Member member);

    Member modifyMember(Member member);
}