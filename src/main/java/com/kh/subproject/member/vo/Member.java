package com.kh.subproject.member.vo;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;

@Data
@Entity
@AllArgsConstructor
@NoArgsConstructor
public class Member {

    @Id
    @GeneratedValue(
            strategy = GenerationType.SEQUENCE,
            generator = "MEMBER_SEQUENCE"
    )
    private Long mno;

    @Column(unique = true, nullable = false)
    private String mid;

    @Column(unique = true, nullable = false)
    private String password;

    private String name;

    private String email;

    private String status;

    private String admin;

}
