package com.kh.subproject.board.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.kh.subproject.board.vo.Article;
import com.kh.subproject.board.vo.Board;

public interface BoardRepository extends JpaRepository<Board, Integer>{
	
}
